package ru.tinkoff.academy.tinkofflibrary.form;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.academy.tinkofflibrary.form.dto.FormDto;

@RestController
@RequestMapping("/form")
@RequiredArgsConstructor
public class FormController {
    private final FormService formService;

    @Operation(summary = "Get form by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Form found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Form.class))}),
            @ApiResponse(responseCode = "404", description = "Form not found",
                    content = @Content)})
    @GetMapping(value = "/{id}", produces = "application/json")
    public Form getForm(@Parameter(description = "id of form to be gotten") @PathVariable Long id) {
        return formService.findById(id);
    }

    @Operation(summary = "Update form by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Form updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Form.class))}),
            @ApiResponse(responseCode = "404", description = "Form not found or board/note component of form not found",
                    content = @Content)})
    @PutMapping(value = "/{id}", produces = "application/json", consumes = "application/json")
    public Form updateForm(
            @Parameter(description = "id of form to be updated") @PathVariable Long id,
            @RequestBody FormDto formDto
    ) {
        return formService.updateById(id, formDto);
    }

    @Operation(summary = "Create new form")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Form created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Form.class))}),
            @ApiResponse(responseCode = "404", description = "board/note component of form not found",
                    content = @Content)})
    @PostMapping(produces = "application/json", consumes = "application/json")
    public Form createForm(@RequestBody FormDto formDto) {
        return formService.save(formDto);
    }

    @Operation(summary = "Remove form by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Form removed",
                    content = @Content)})
    @DeleteMapping(value = "/{id}")
    public void removeForm(@PathVariable Long id) {
        formService.deleteById(id);
    }
}
