package ru.tinkoff.academy.tinkofflibrary.form.board;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.academy.tinkofflibrary.form.board.dto.BoardDto;
import ru.tinkoff.academy.tinkofflibrary.form.board.task.TaskService;

@RestController
@RequestMapping("/board")
@RequiredArgsConstructor
public class BoardController {
    private final BoardService boardService;

    @GetMapping(value = "/{id}", produces = "application/json")
    public Board getBoard(@PathVariable Long id) {
        return boardService.findById(id);
    }

    @PutMapping(value = "/{id}", produces = "application/json", consumes = "application/json")
    public Board updateBoard(@PathVariable Long id, @RequestBody BoardDto boardDto) {
        return boardService.updateById(id, boardDto);
    }

    @PostMapping(produces = "application/json", consumes = "application/json")
    public Board createBoard(@RequestBody BoardDto boardDto) {
        return boardService.save(boardDto);
    }

    @DeleteMapping(value = "/{id}")
    public void removeBoard(@PathVariable Long id) {
        boardService.deleteById(id);
    }
}
