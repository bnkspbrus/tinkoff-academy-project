package ru.tinkoff.academy.tinkofflibrary.form.note;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.academy.tinkofflibrary.form.note.dto.NoteDto;
import ru.tinkoff.academy.tinkofflibrary.tag.TagService;

import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/note")
public class NoteController {
    private final NoteService noteService;
    private final TagService tagService;

    @GetMapping(value = "/{id}", produces = "application/json")
    public Note getNote(@PathVariable Long id) {
        return noteService.findById(id);
    }

    @PutMapping(value = "/{id}", produces = "application/json", consumes = "application/json")
    public Note updateNote(@PathVariable Long id, @RequestBody NoteDto noteDto) {
        return noteService.updateById(id, noteDto);
    }

    @PostMapping(produces = "application/json", consumes = "application/json")
    public Note createNote(@RequestBody NoteDto noteDto) {
        return noteService.save(noteDto);
    }

    @DeleteMapping(value = "/{id}")
    public void removeNote(@PathVariable Long id) {
        noteService.deleteById(id);
    }

    @Operation(summary = "Search notes by tag")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Notes found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Note.class))}),
            @ApiResponse(responseCode = "404", description = "Tag not found",
                    content = @Content)})
    @GetMapping(value = "/search")
    public Set<Note> search(@RequestParam(name = "tag") String tag) {
        return tagService.findByName(tag).getNotes();
    }
}
