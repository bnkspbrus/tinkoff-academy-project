package ru.tinkoff.academy.tinkofflibrary.form.note;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.tinkoff.academy.tinkofflibrary.form.note.dto.NoteDto;
import ru.tinkoff.academy.tinkofflibrary.tag.TagService;

@Service
@RequiredArgsConstructor
public class NoteService {
    private final NoteRepository noteRepository;
    private final TagService tagService;

    public Note findById(Long id) {
        return noteRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Note with id " + id + " not found")
        );
    }

    public Note updateById(Long id, NoteDto noteDto) {
        if (!noteRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Note with id " + id + " not found");
        }
        Note note = mapToNote(noteDto);
        note.setId(id);
        return noteRepository.save(note);
    }

    public Note save(NoteDto noteDto) {
        Note note = mapToNote(noteDto);
        return noteRepository.save(note);
    }

    public void deleteById(Long id) {
        noteRepository.deleteById(id);
    }

    private Note mapToNote(NoteDto noteDto) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        Note note = mapper.map(noteDto, Note.class);
        if (noteDto.getNoteId() != null) {
            note.setNote(findById(noteDto.getNoteId()));
        }
        if (noteDto.getTagId() != null) {
            note.setTag(tagService.findById(noteDto.getTagId()));
        }
        return note;
    }
}
