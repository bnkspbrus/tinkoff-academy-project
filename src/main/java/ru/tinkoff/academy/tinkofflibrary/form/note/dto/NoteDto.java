package ru.tinkoff.academy.tinkofflibrary.form.note.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NoteDto {
    private String name;
    private String text;
    private LocalDateTime creationTime;
    private LocalDateTime modificationTime;
    private Long noteId;
    private Long tagId;
}
