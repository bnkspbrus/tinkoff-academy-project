package ru.tinkoff.academy.tinkofflibrary.form.pull;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PullRepository extends CrudRepository<Pull, Long> {
}
