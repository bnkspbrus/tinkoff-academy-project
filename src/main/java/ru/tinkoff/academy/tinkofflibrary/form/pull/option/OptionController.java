package ru.tinkoff.academy.tinkofflibrary.form.pull.option;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.academy.tinkofflibrary.form.pull.option.dto.OptionDto;

@RestController
@RequestMapping("/option")
@RequiredArgsConstructor
public class OptionController {
    private final OptionService optionService;

    @GetMapping(value = "/{id}", produces = "application/json")
    public Option getOption(@PathVariable Long id) {
        return optionService.findById(id);
    }

    @PutMapping(value = "/{id}", produces = "application/json", consumes = "application/json")
    public Option updateOption(@PathVariable Long id, @RequestBody OptionDto optionDto) {
        return optionService.updateById(id, optionDto);
    }

    @PostMapping(produces = "application/json", consumes = "application/json")
    public Option createOption(@RequestBody OptionDto optionDto) {
        return optionService.save(optionDto);
    }

    @DeleteMapping(value = "/{id}")
    public void removeOption(@PathVariable Long id) {
        optionService.deleteById(id);
    }

    @Operation(summary = "Increment vote counter of option")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Vote counter incremented",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Option not found",
                    content = @Content)})
    @PutMapping(value = "/vote/{id}")
    public void vote(@PathVariable Long id) {
        optionService.vote(id);
    }
}
