package ru.tinkoff.academy.tinkofflibrary.form.pull.option;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.tinkoff.academy.tinkofflibrary.form.pull.PullService;
import ru.tinkoff.academy.tinkofflibrary.form.pull.option.dto.OptionDto;

@Service
@RequiredArgsConstructor
public class OptionService {
    private final OptionRepository optionRepository;
    private final PullService pullService;

    public Option findById(Long id) {
        return optionRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Option with id " + id + " not found")
        );
    }

    public Option updateById(Long id, OptionDto optionDto) {
        if (!optionRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Option with id " + id + " not found");
        }
        Option option = mapToOption(optionDto);
        option.setId(id);
        return optionRepository.save(option);
    }

    public Option save(OptionDto optionDto) {
        Option option = mapToOption(optionDto);
        return optionRepository.save(option);
    }

    public void deleteById(Long id) {
        optionRepository.deleteById(id);
    }

    public void vote(Long id) {
        Option option = findById(id);
        option.setVoteCounter(option.getVoteCounter() + 1);
        optionRepository.save(option);
    }

    private Option mapToOption(OptionDto optionDto) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        Option option = mapper.map(optionDto, Option.class);
        if (optionDto.getPullId() != null) {
            option.setPull(pullService.findById(optionDto.getPullId()));
        }
        return option;
    }
}
