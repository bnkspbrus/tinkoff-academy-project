package ru.tinkoff.academy.tinkofflibrary.form.pull.option.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OptionDto {
    private String name;
    private Integer voteCounter;
    private Long pullId;
}
