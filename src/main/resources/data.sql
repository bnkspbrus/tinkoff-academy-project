insert into tag (name)
values ('#a');
insert into tag (name)
values ('#b');
insert into tag (name)
values ('#c');
insert into tag (name)
values ('#d');
insert into tag (name)
values ('#e');

insert into note (name, text, creation_time, modification_time, tag_id)
values ('note_1', 'text_1', '2222-12-22 12:12:12', '2222-12-22 12:12:12', 4);
insert into note (name, text, creation_time, modification_time, note_id, tag_id)
values ('note_2', 'text_2', '2222-12-22 12:12:12', '2222-12-22 12:12:12', 1, 5);
insert into note (name, text, creation_time, modification_time, note_id, tag_id)
values ('note_3', 'text_3', '2222-12-22 12:12:12', '2222-12-22 12:12:12', 1, 2);
insert into note (name, text, creation_time, modification_time, note_id, tag_id)
values ('note_4', 'text_4', '2222-12-22 12:12:12', '2222-12-22 12:12:12', 3, 2);
insert into note (name, text, creation_time, modification_time, note_id, tag_id)
values ('note_5', 'text_5', '2222-12-22 12:12:12', '2222-12-22 12:12:12', 4, 3);

insert into board (name, description, creation_time, modification_time)
values ('board_1', 'desc_1', '2000-10-20 10:10:10', '2222-12-22 12:12:12');
insert into board (name, description, creation_time, modification_time)
values ('board_2', 'desc_2', '2000-10-20 10:10:10', '2222-12-22 12:12:12');
insert into board (name, description, creation_time, modification_time)
values ('board_3', 'desc_3', '2000-10-20 10:10:10', '2222-12-22 12:12:12');
insert into board (name, description, creation_time, modification_time)
values ('board_4', 'desc_4', '2000-10-20 10:10:10', '2222-12-22 12:12:12');
insert into board (name, description, creation_time, modification_time)
values ('board_5', 'desc_5', '2000-10-20 10:10:10', '2222-12-22 12:12:12');

insert into form (form_type, board_id)
values ('BOARD', 1);
insert into form (form_type, note_id)
values ('NOTE', 1);
insert into form (form_type, note_id)
values ('NOTE', 2);
insert into form (form_type, board_id)
values ('BOARD', 2);
insert into form (form_type, note_id)
values ('NOTE', 3);
insert into form (form_type, board_id)
values ('BOARD', 5);
insert into form (form_type, note_id)
values ('NOTE', 3);
insert into form (form_type, board_id)
values ('BOARD', 4);
insert into form (form_type, note_id)
values ('NOTE', 5);
insert into form (form_type, note_id)
values ('NOTE', 2);

insert into status (name)
values ('A');
insert into status (name)
values ('B');
insert into status (name)
values ('C');
insert into status (name)
values ('E');
insert into status (name)
values ('F');

insert into board_to_status (board_id, status_id)
values (1, 1);
insert into board_to_status (board_id, status_id)
values (2, 1);
insert into board_to_status (board_id, status_id)
values (2, 2);
insert into board_to_status (board_id, status_id)
values (1, 5);
insert into board_to_status (board_id, status_id)
values (2, 4);
insert into board_to_status (board_id, status_id)
values (4, 4);
insert into board_to_status (board_id, status_id)
values (5, 4);
insert into board_to_status (board_id, status_id)
values (5, 1);

insert into task(name, description, creation_time, modification_time, tag_id, status_id, board_id)
values ('task_1', 'desc_1', '2222-12-22 12:12:12', '2222-12-22 12:12:12', 1, 1, 1);
insert into task(name, description, creation_time, modification_time, tag_id, status_id, board_id)
values ('task_2', 'desc_2', '2222-12-22 12:12:12', '2222-12-22 12:12:12', 2, 3, 4);
insert into task(name, description, creation_time, modification_time, tag_id, status_id, board_id)
values ('task_3', 'desc_3', '2222-12-22 12:12:12', '2222-12-22 12:12:12', 1, 3, 1);
insert into task(name, description, creation_time, modification_time, tag_id, status_id, board_id)
values ('task_4', 'desc_4', '2222-12-22 12:12:12', '2222-12-22 12:12:12', 4, 5, 1);
insert into task(name, description, creation_time, modification_time, tag_id, status_id, board_id)
values ('task_5', 'desc_5', '2222-12-22 12:12:12', '2222-12-22 12:12:12', 5, 5, 2);

insert into pull(name, description)
values ('pull_1', 'desc_1');
insert into pull(name, description)
values ('pull_2', 'desc_2');
insert into pull(name, description)
values ('pull_2', 'desc_2');
insert into pull(name, description)
values ('pull_2', 'desc_2');
insert into pull(name, description)
values ('pull_2', 'desc_2');

insert into option(name, vote_counter, pull_id)
values ('option_1', 0, 1);
insert into option(name, vote_counter, pull_id)
values ('option_2', 10, 1);
insert into option(name, vote_counter, pull_id)
values ('option_3', 5, 1);
insert into option(name, vote_counter, pull_id)
values ('option_4', 5, 2);
insert into option(name, vote_counter, pull_id)
values ('option_5', 15, 2);

--insert into task (name, description, creation_time, modification_time, username, )
